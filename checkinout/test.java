package checkinout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Vector;

public class test {

	public static void main(String[] args) {
		
//		//get asset
		Asset asset = SnipeIT.getAsset(10453);
		SnipeIT.clearReservation(asset);
		try {
			SnipeIT.reserveAsset("corbinv", "2019-08-29", asset);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		//get user
		User user = SnipeIT.getUser("corbinv");
//		System.out.println(user.getName());
		
//		//checkout asset
//		System.out.println(SnipeIT.checkOut(asset, user));
		
//		//checkin asset
//		System.out.println(SnipeIT.checkIn(asset));
		
		//ldap auth
//		boolean ldapAuth = SnipeLDAP.userAuth("notifications", "Carna1916");
//		System.out.println(ldapAuth);
		
		//update asset
//		Asset.CustomFields cf = asset.getCustomFields();
//		cf.setReserved(true);
//		asset.setCustomFields(cf);
//		SnipeIT.updateAsset(asset);
		
		//query params test
//		QueryParams qp = new QueryParams(QueryParams.ASSET);
//		qp.setLimitString("1");
//		SnipeIT.searchAssets(qp);
//		qp = new QueryParams(QueryParams.USER);
//		ArrayList<JsonElement> x = SnipeIT.searchAssets(qp);
//		for (JsonElement js : x) {
//			System.out.println(js.toString());
//		}
		
		//collab scrape test
		try {
			Vector<Asset> a = SnipeIT.scrapeCollabLaptops("Collaborative");
			for (Asset lol : a) {
				SnipeIT.clearReservation(lol);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//
		try {
			Vector<Asset> availableAssets = SnipeIT.checkAvailability("2019-08-29", "Collaborative");
			System.out.println(availableAssets.size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
//		try {
//			Vector<Asset> returnables = SnipeIT.checkReturnables("corbinv", "Collaborative");
//			for (Asset a : returnables) {
//				System.out.println(a.toString());
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		try {
//			SnipeIT.autoCheckOut("Collaborative");
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		
	}
}

package checkinout;

import java.util.Date;

public class Asset{
	private int asset_tag;
	private int id;
	private String notes;
	private Date checkOutDate = null;
	private boolean canBeCheckedOut;
	private StatusLabel status_label;
	private CustomFields custom_fields;
	private AssignedTo assigned_to;
	
	
	//inner classes for nested json properties 
	public class StatusLabel{
		private String status_id;
		private String status_meta;
		
		public String getStatusID() {
			return status_id;
		}
		public void setStatusID(String statusID) {
			this.status_id = statusID;
		}
		public String getStatusMeta() {
			return status_meta;
		}
		public void setStatusMeta(String statusMeta) {
			this.status_meta = statusMeta;
		}
	}
	
	public class AssignedTo{
		private String username;
		private int id;
		
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public int getID() {
			return id;
		}
		public void setID(int id) {
			this.id = id;
		}
	}
	
	public class CustomFields{
		private boolean _snipeit_reserved_4;
		private String _snipeit_reserved_by_3;
		private String _snipeit_reserved_date_2;
		
		public boolean isReserved() {
			return _snipeit_reserved_4;
		}
		public void setReserved(boolean reserved) {
			this._snipeit_reserved_4 = reserved;
		}
		public String getReservedBy() {
			return _snipeit_reserved_by_3;
		}
		public void setReservedBy(String reservedBy) {
			this._snipeit_reserved_by_3 = reservedBy;
		}
		public String getReservedDate() {
			return _snipeit_reserved_date_2;
		}
		public void setReservedDate(String reservedDate) {
			this._snipeit_reserved_date_2 = reservedDate;
		}
	}
	/*************************************************************************************************************/
	//set & get methods
	//Tag
	public int getTag() {
		return asset_tag;
	}
	public void setTag(int asset_tag) {
		this.asset_tag = asset_tag;
	}
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	//Notes
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getNotes() {
		return notes;
	}
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public boolean isCanBeCheckedOut() {
		return canBeCheckedOut;
	}
	public void setCanBeCheckedOut(boolean canBeCheckedOut) {
		this.canBeCheckedOut = canBeCheckedOut;
	}
	//Status Name
	public void setStatusID(StatusLabel statusLabel) {
		this.status_label = statusLabel;
	}
	public StatusLabel getStatusLabel() {
		return status_label;
	}
	
	//Assigned To
	public void setAssignedTo(AssignedTo assignedTo) {
		this.assigned_to = assignedTo;
	}
	public AssignedTo getAssignedTo() {
		return assigned_to;
	}

	//Reserved
	public void setCustomFields(CustomFields customFields) {
		this.custom_fields = customFields;
	}
	public CustomFields getCustomFields() {
		return custom_fields;
	}
	//end set & get methods
}

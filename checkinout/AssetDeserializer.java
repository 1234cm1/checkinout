package checkinout;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class AssetDeserializer implements JsonDeserializer<Asset> {
	@Override
	public Asset deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException{
		
		//deserialize
		JsonObject obj = json.getAsJsonObject();
		int tag = obj.get("asset_tag").getAsInt();
		int id = obj.get("id").getAsInt();
		
		String notes = null;
		if(obj.get("notes") != null && !obj.get("notes").isJsonNull()) {
			 notes = obj.get("notes").getAsString();
		}
		
		//get status information
		JsonElement statusElem = obj.get("status_label");
		String statusID = statusElem.getAsJsonObject().get("id").getAsString();
		String statusMeta = statusElem.getAsJsonObject().get("status_meta").getAsString();

		//get assigned ID number
		int assignedID = -1;
		JsonElement assiElem = obj.get("assigned_to");
		if (assiElem != null && !assiElem.isJsonNull()) {
			if (assiElem.getAsJsonObject().get("id") != null) {
				assignedID = assiElem.getAsJsonObject().get("id").getAsInt();
			}
		}
		
		//get reserved information from custom fields
		JsonElement reservedFields = obj.get("custom_fields");
		boolean reserved = false;
		String reservedBy = null;
		String reservedDate = null;
		if (!reservedFields.isJsonArray()) {
			JsonElement resBool = reservedFields.getAsJsonObject().get("Reserved");
			if (resBool != null && !resBool.isJsonNull() && resBool.getAsJsonObject().get("value") != null 
					&& !resBool.getAsJsonObject().get("value").isJsonNull()) {
				if (resBool.getAsJsonObject().get("value").getAsInt() == 1) {
					reserved = true;
				} else {
					reserved = false;
				}
			}
			JsonElement resBy = reservedFields.getAsJsonObject().get("Reserved By");
			if (resBy != null && !resBy.isJsonNull()) {
				if (resBy.getAsJsonObject().get("value") != null && !resBy.getAsJsonObject().get("value").isJsonNull()) {
					reservedBy = resBy.getAsJsonObject().get("value").getAsString();
				}
			}
			JsonElement resDate = reservedFields.getAsJsonObject().get("Date Reserved");
			if (resDate != null && !resDate.isJsonNull()) {
				if (resDate.getAsJsonObject().get("value") != null && !resDate.getAsJsonObject().get("value").isJsonNull()) {
					reservedDate = resDate.getAsJsonObject().get("value").getAsString();
				}
			}
		}

		
		//create asset and apply retrieved params
		final Asset asset = new Asset();
		asset.setTag(tag);
		asset.setID(id);
		asset.setNotes(notes);
		
		Asset.StatusLabel statusLabel = asset.new StatusLabel();
		statusLabel.setStatusID(statusID);
		statusLabel.setStatusMeta(statusMeta);
		asset.setStatusID(statusLabel);
		
		Asset.AssignedTo assignedTo = asset.new AssignedTo();
		assignedTo.setID(assignedID);
		asset.setAssignedTo(assignedTo);
		
		Asset.CustomFields customFields = asset.new CustomFields();
		customFields.setReserved(reserved);
		customFields.setReservedBy(reservedBy);
		customFields.setReservedDate(reservedDate);
		asset.setCustomFields(customFields);

		return asset;
	}
	
	//Gson: pass in json, calls deserializer
	public static Gson gsonAssetCreator() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Asset.class, new AssetDeserializer());
		Gson gson = builder.create();
		return gson;
	}	
}
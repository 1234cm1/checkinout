package checkinout;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class SnipeLDAP {
	
	private final static String sp = "com.sun.jndi.ldap.LdapCtxFactory";
	private final static String ldapURL = "ldap://192.168.0.20/dc=nurses,dc=local";
	private final static String base = "OU=CARNA Users,DC=nurses,DC=local";
	
	public static Hashtable auth(String creds, String pass) {
		/* Passes hashtable out, if it is not authenticated, using the hashtable will cause a security error
		 * string to be printed.
		 * Creds requires the entire designated Name of the user in LDAP
		 */
		Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, sp);
        env.put(Context.PROVIDER_URL, ldapURL);
        env.put(Context.REFERRAL, "follow");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, creds);
        env.put(Context.SECURITY_CREDENTIALS, pass);

        try {
            Context ctx = new InitialContext(env);
            NamingEnumeration enm = ctx.list("");
            //while there is still data, keep receiving
            while (enm.hasMore()) {
                enm.next();
            }

            enm.close();
            ctx.close();
        } catch (NamingException e) {
            System.out.println(e.getMessage());
            env.clear();
        }
        return env; 
	}
	
	//if the username is incorrect, this will return null
	public static String search(String username, Hashtable env) {
		String dn = null;
		
		SearchControls ctl = new SearchControls();
		ctl.setSearchScope(SearchControls.SUBTREE_SCOPE);

		try {
			// Create initial context
			DirContext ctx = new InitialDirContext(env);
			//directly search username
			NamingEnumeration answer = ctx.search("", "(&(objectClass=*)(sAMAccountName=" + username + "))", ctl);

			// Print the answer
			dn = printSearchEnumeration(answer);

			// Close the context when we're done
			ctx.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return dn;
	}
	
	//get dn from search result
	public static String printSearchEnumeration(NamingEnumeration retEnum) {
		String dn = null;
		try {
			while (retEnum.hasMore()) {
				SearchResult sr = (SearchResult) retEnum.next();
				dn = sr.getNameInNamespace();
//				System.out.println(">>" + dn);
			}
		} 
		catch (NamingException e) {
			e.printStackTrace();
		}
		return dn;
	}
	
	//
	public static boolean userAuth(String username, String password) {
		boolean authed = false;
		
		//In order to do a user search, have to auth against LDAP TODO: obfuscate Jarvis DN
		Hashtable jarAuthed = SnipeLDAP.auth("CN=Jarvis,OU=IT Infrastructure,OU=Corporate Services,OU=Secure Office Users,OU=CARNA Users,DC=nurses,DC=local", "Carna1916");
		String dn = SnipeLDAP.search(username, jarAuthed); //perform search
		if (dn != null) { //given that the username is valid
			Hashtable userAuth = SnipeLDAP.auth(dn, password); //try to authenticate with user's DN
			if (!userAuth.isEmpty()) { //hash cleared when password incorrect, so when not empty, is authed
				authed = true;
			}
		}
		return authed;
	}
}

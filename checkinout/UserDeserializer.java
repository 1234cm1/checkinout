package checkinout;

import java.lang.reflect.Type;

import com.google.gson.*;

public class UserDeserializer implements JsonDeserializer<User> {
	@Override
	public User deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) 
			throws JsonParseException{
		
		String id = null;
		String username = null;
		String name = null;
		String first_name = null;
		String last_name = null;
		String email = null;
		
		JsonObject obj = json.getAsJsonObject();
		if (obj != null && !obj.isJsonNull()) { //check json is not empty
			String total = obj.get("total").getAsString();
			if (total.equals("1")) { //given that search only returns one result
				JsonArray rows = obj.get("rows").getAsJsonArray(); 
				JsonObject jsonUser = rows.get(0).getAsJsonObject();
				if (jsonUser != null && !jsonUser.isJsonNull()) { //check that user is not empty
					id = jsonUser.get("id").getAsString();
					username = jsonUser.get("username").getAsString();
					name = jsonUser.get("name").getAsString();
					first_name = jsonUser.get("first_name").getAsString();
					last_name = jsonUser.get("last_name").getAsString();
					email = jsonUser.get("email").getAsString();
				}		
			}
		}
		
		//set user vals
		final User user = new User();
		user.setID(id);
		user.setUsername(username);
		user.setName(name);
		user.setFirstName(first_name);
		user.setLastName(last_name);
		user.setEmail(email);
		return user;
	}
	
	//Gson: pass in json, calls deserializer
	public static Gson gsonUserCreator() {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(User.class, new UserDeserializer());
		Gson gson = builder.create();
		return gson;
	}
	
}

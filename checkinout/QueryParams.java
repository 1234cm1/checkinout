package checkinout;

public class QueryParams {
	private String params;
	private boolean isAsset;
	private String limitString = "50";
	private String offsetString = "0";
	private String searchString;
	private String ordNumString;
	private String sortString = "created_at";
	
	public static final boolean ASSET = true;
	public static final boolean USER = false;
	/*
	 * Sortable Columns
	 * id               Asset ids
	 * name             Asset names
	 * asset_tag        Asset unique asset tag
	 * serial           Asset serial numbers
	 * model            Name of the associated asset model
	 * model_number     Model number of the associated asset model
	 * last_checkout    Date the asset was last checked out
	 * category         Name of the asset model's associated category
	 * manufacturer     Name of the asset's manufacturer
	 * notes            Asset notes
	 * expected_checkin Expected checkin date
	 * order_number     Order number associated with the asset
	 * companyName      Company name associated with the asset, if applicable
	 * location         Name of the location of the asset, either the default Ready to Deploy location, or the assigned user's location
	 * image            Name of the optional image file associated with the asset
	 * status_label     Name of the status label associated with the asset
	 * assigned_to      Name of the person the asset is assigned to
	 * created_at       Date the asset was created
	 * purchase_date    Date the asset was purchased
	 * purchase_cost    Purchase cost of the asset
	 */
	private String orderString = "desc";
	private String modelIDString;
	private String categoryIDString;
	private String manufacturerIDString;
	private String companyIDString;
	private String locationIDString;
	private String statusString;
	private String statusIDString;
	private String groupIDString;
	private String departmentIDString;
	private boolean deleted = false;;
	
	public QueryParams(boolean isAsset) {
		//indicate if params are for asset[true] or user[false]
		this.isAsset = isAsset;
	}
	
	public String getParams() {
		/* Will create url parameters in correct order for asset of user.
		 * If fields have not already been initialized, it will generate default
		 * URL parameters. 
		 */
		if (isAsset) {
			params = "";
			appendLimit(true);
			appendOffset();
			appendSearch(false);
			appendOrdNum();
			appendSort();
			appendOrder();
			appendModel();
			appendCategory();
			appendManufacturer();
			appendCompany();
			appendLocation();
			appendStatus();
			appendStatusID();
		} else {
			params = "";
			if (searchString != null) { 
				appendSearch(true);
				appendLimit(false);
			} else {
				appendLimit(true);
			}
			appendOffset();
			appendOrdNum();
			appendSort();
			appendOrder();
			appendGroup();
			appendCompany();
			appendDepartment();
			appendDeleted();
		}
		return params;
	}
	/*************************************************************************************************************/
	//appenders, only used for final concat
	private void appendFirst(String param, String paramSetting) {
		if(paramSetting != null) {
			params += "?" + param +"=" + paramSetting;
		}
	}
	private void appendFilter(String param, String paramSetting) {
		if (paramSetting != null) {
			params += "&" + param +"=" + paramSetting;
		}
	}
	
	private void appendLimit(boolean first) {
		if (first) {
			appendFirst("limit", limitString);
		} else {
			appendFilter("limit", limitString);
		}
	}
	private void appendOffset() {
		appendFilter("offset", offsetString);
	}
	private void appendSearch(boolean first) {
		if (first) {
			appendFirst("search", searchString);
		} else {
			appendFilter("search", searchString);
		}
	}
	private void appendOrdNum() {
		appendFilter("order_number", ordNumString);
	}
	private void appendSort() {
		appendFilter("sort", sortString);
	}
	private void appendOrder() {
		appendFilter("order", orderString);
	}
	private void appendModel() {
		appendFilter("model_id", modelIDString);
	}
	private void appendManufacturer() {
		appendFilter("manufacturer", manufacturerIDString);
	}
	private void appendCompany() {
		appendFilter("company", sortString);
	}
	private void appendCategory() {
		appendFilter("category_id", categoryIDString);
	}
	private void appendLocation() {
		appendFilter("location_id", locationIDString);
	}
	private void appendStatus() {
		appendFilter("status", statusString);
	}
	private void appendStatusID() {
		appendFilter("status_id", statusIDString);
	}
	private void appendGroup() {
		appendFilter("group_id", groupIDString);
	}
	private void appendDepartment() {
		appendFilter("department_id", departmentIDString);
	}
	private void appendDeleted() {
		appendFilter("deleted", String.valueOf(deleted));
	}
	/*************************************************************************************************************/
	//setters and getters
	public boolean isAsset() {
		return isAsset;
	}
	public void setAsset(boolean isAsset) {
		this.isAsset = isAsset;
	}

	public String getLimitString() {
		return limitString;
	}

	public void setLimitString(String limitString) {
		this.limitString = limitString;
	}

	public String getOffsetString() {
		return offsetString;
	}

	public void setOffsetString(String offsetString) {
		this.offsetString = offsetString;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getOrdNumString() {
		return ordNumString;
	}

	public void setOrdNumString(String ordNumString) {
		this.ordNumString = ordNumString;
	}

	public String getSortString() {
		return sortString;
	}

	public void setSortString(String sortString) {
		this.sortString = sortString;
	}

	public String getOrderString() {
		return orderString;
	}

	public void setOrderString(String orderString) {
		this.orderString = orderString;
	}

	public String getModelIDString() {
		return modelIDString;
	}

	public void setModelIDString(String modelIDString) {
		this.modelIDString = modelIDString;
	}

	public String getCategoryIDString() {
		return categoryIDString;
	}

	public void setCategoryIDString(String categoryIDString) {
		this.categoryIDString = categoryIDString;
	}

	public String getManufacturerIDString() {
		return manufacturerIDString;
	}

	public void setManufacturerIDString(String manufacturerIDString) {
		this.manufacturerIDString = manufacturerIDString;
	}

	public String getCompanyIDString() {
		return companyIDString;
	}

	public void setCompanyIDString(String companyIDString) {
		this.companyIDString = companyIDString;
	}

	public String getLocationIDString() {
		return locationIDString;
	}

	public void setLocationIDString(String locationIDString) {
		this.locationIDString = locationIDString;
	}

	public String getStatusString() {
		return statusString;
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public String getStatusIDString() {
		return statusIDString;
	}

	public void setStatusIDString(String statusIDString) {
		this.statusIDString = statusIDString;
	}

	public String getGroupIDString() {
		return groupIDString;
	}

	public void setGroupIDString(String groupIDString) {
		this.groupIDString = groupIDString;
	}

	public String getDepartmentIDString() {
		return departmentIDString;
	}

	public void setDepartmentIDString(String departmentIDString) {
		this.departmentIDString = departmentIDString;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}

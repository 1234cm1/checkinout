package checkinout;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import checkinout.Asset.AssignedTo;

public class SnipeIT{
	public static final String url = "http://edmsnipeit/api/v1";
	
	//API request Strings
	private static final String HEAD_A = "accept";
	private static final String HEAD_J = "application/json";
	private static final String HEAD_U = "authorization";
	private static final String HEAD_C = "content-type";
	
	//Jarvis' api key TODO: obfuscate this information
	private static final String apiKey = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjlhZDJmNTg1MjA5NTI0ZmNkYjc0ZjE5ZjI4YTM3YTA3MmMxMTYyNmE1OTk4M2FiYTJlY2Y4ZDFkMDdmYmY5MmNiZjNhNzg3YzhhOTJlNDc4In0.eyJhdWQiOiIxIiwianRpIjoiOWFkMmY1ODUyMDk1MjRmY2RiNzRmMTlmMjhhMzdhMDcyYzExNjI2YTU5OTgzYWJhMmVjZjhkMWQwN2ZiZjkyY2JmM2E3ODdjOGE5MmU0NzgiLCJpYXQiOjE1NjU5NzE1MjMsIm5iZiI6MTU2NTk3MTUyMywiZXhwIjoxNTk3NTkzOTIzLCJzdWIiOiIxNTAiLCJzY29wZXMiOltdfQ.C2J4gejRuSigaeIZrXDor00ftDw9iU0cjCmBFCp8QEQYHv2Nar-Alk8CuG4btYj2KOC9_QpT3onHqm6Diwx3Qavmzd3oXxm5Nkt6DPegK6PIwor8C3F5yaZhNNmNAEC8OPRvujuH0wvakc5PtrdvcFdss7D58hK-UakH4QmEaohs2S_Reqsu8Q4Rr7fvN3NstQTjtALVlLT7I21mJSGHxoibyvXOc_RlrnbZVdI9tlDtBHtdItr2-8Qp_MQq-Cz13COvwiF00xQxrHM_CJGWT5IjqSxNXL2nCQOx7jOVp9wAn2bnYSGKh2axs9FCMvzX0bTDFtkJODBjqc5cyqRKLnBe1KtE92AiWYof-r9cIQ6AxU_1u47IfbcZIkvbdne5DVnl3gxFXP0f0NfTcrYz96qYrMgnxCTBaiq7RwhXSWEjSki4ep-4BRfSobAFPKjVqh8Sd22j9_IzHsj9Ms2wbcXXGf8RamIgvso7payiwqL_eeOHCPr5BnXfY8_Wa2bYYA_ZSCRuCMGgCqxdULMXj-psHKLcHKpdymGd4DvBo5M60cejRItdtxV3jJjLWhGTcLjNshjhtLATmE4ddeiTrag9Cjxu2z6KFw6q2TT8YwOklnenFzXTKQKCJnLNwpEQ4sluVOMHuv9gqaX96BAuJ0dE0ABVbxxP9KBxFr0_xQ0";
	
	//URL generators
	public static final String hardwareURL() {
		return url + "/hardware";
	}
	public static final String usersURL() {
		return url + "/users";
	}
	public static String byTagURL(int tag) {
		return hardwareURL()+ "/bytag/" + tag;
	}
	public static String idURL(int id) {
		return hardwareURL() + "/" + id;
	}
	public static String checkInURL(int id) {
		return hardwareURL() + "/" + id + "/checkin";
	}
	public static String checkOutURL(int id) {
		return hardwareURL() + "/" + id + "/checkout";
	}
	public static String searchURL(String username) {
		return url + "/users?search=" + username;
	}
	
	public static void checkDateFormat(String date) throws IOException{
		if (!date.matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")) {
			throw new IOException("Date format incorrect. Use: YYYY-MM-DD");
		}
	}
	/*************************************************************************************************************/
	//HTTP request Methods
	public static JsonElement sendRQ(String completeURL, String mode, String postJson) throws Exception{
		JsonElement RQ = null;
		
		//set up Http Request
		RequestBuilder rqBuilder = null;
		if (mode.equalsIgnoreCase("GET")) {
			rqBuilder = RequestBuilder.get();
		} else if (mode.equalsIgnoreCase("POST")) {
			rqBuilder = RequestBuilder.post();
		} else if (mode.equalsIgnoreCase("PATCH")) {
			rqBuilder = RequestBuilder.patch();
		}
		//set headers
		if (rqBuilder != null) {
			rqBuilder
				.setUri(completeURL)
				.addHeader(HEAD_U, apiKey)
				.addHeader(HEAD_A, HEAD_J)
				.addHeader(HEAD_C, HEAD_J)
				.setEntity(new StringEntity(postJson, ContentType.APPLICATION_JSON));
		}
		
		//build and then execute request
		HttpUriRequest request = rqBuilder.build();
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);
		//Prints url and response message and code
		System.out.println(completeURL + " | " + response.getStatusLine().getStatusCode() + ": " 
				+ response.getStatusLine().getReasonPhrase());

		if (response.getStatusLine().getStatusCode() == 200) {

			//better reader for debugging
//			try (BufferedReader br = new BufferedReader(
//					new InputStreamReader(response.getEntity().getContent(), "utf-8"))) {
//				StringBuilder res = new StringBuilder();
//				String responseLine = null;
//				while ((responseLine = br.readLine()) != null) {
//					res.append(responseLine.trim());
//				}
//				System.out.println(res.toString());
//			}

			//reader that just displays error message 
			JsonParser jp = new JsonParser();
			InputStreamReader data = new InputStreamReader(response.getEntity().getContent()); //read response 
			JsonReader jr = new JsonReader(data); //give response strem to Json reader
			jr.setLenient(true); //like the expetion says to accept the malformed json
			JsonElement root = jp.parse(jr); //parse Json from stream
//			System.out.println(root.toString());
			String errorCheck[] = root.toString().replaceAll("[{}\"]", "").split(",");
			if (!errorCheck[0].equals("status:error")) {
				RQ = root;
			} else {
				System.out.println(errorCheck[1]); //error message from SnipeIT
			}
		}
		return RQ;
	}
	
	public static JsonElement getRQ(String completeURL) {
		//get request, pass in complete request URL
		JsonElement RQ = null;
		try {
			RQ = sendRQ(completeURL, "GET", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RQ;
	}
	
	public static boolean postRQ(String completeURL, String postJson){
		//post request, pass in complete request URL and json to post
		boolean valid = false;
		JsonElement result = null;
		try {
			result = sendRQ(completeURL, "POST", postJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result != null && !result.isJsonNull()) {
			valid = true;
		}
		return valid;
	}

	public static boolean patchRQ(String completeURL, String postJson){
		//post request, pass in complete request URL and json to post
		boolean valid = false;
		JsonElement result = null;
		try {
			result = sendRQ(completeURL, "PATCH", postJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (result != null && !result.isJsonNull()) {
			valid = true;
		}
		return valid;
	}
	
	/*************************************************************************************************************/
	//Object creators
	public static Asset getAsset(int tag) {
		//creates asset object from SnipeIT data
		JsonElement jsonAsset = getRQ(byTagURL(tag));
		Asset asset = new Asset();
		//if request returns data, run through custom deserializer
		if (jsonAsset != null && !jsonAsset.isJsonNull()) {
			Gson gson = AssetDeserializer.gsonAssetCreator();
			 asset = gson.fromJson(jsonAsset, Asset.class);
		} else {
			System.out.println("Asset retrival failed");
		}
		return asset;
	}
	

	public static User getUser(String username) {
		//creates user object from SnipeIT data
		JsonElement userJson= getRQ(searchURL(username));
		User user = new User();
		//if request returns data, run through custom deserializer
		if (userJson != null && !userJson.isJsonNull()) {
			Gson gson = UserDeserializer.gsonUserCreator();
			 user = gson.fromJson(userJson, User.class);
		} else {
			System.out.println("User retrival failed");
		}
		return user;
	}
	/*************************************************************************************************************/
	//Basic SnipeIT operations 
	
	public static boolean checkIn(Asset asset){
		//return true if asset checked in
		Gson gson = new Gson();
		String postAsset = gson.toJson(asset);
		return postRQ(checkInURL(asset.getID()), postAsset);
	}

	public static boolean checkOut(Asset asset, User user){
		//return true if checked out
		JsonObject obj = new JsonObject();
		obj.addProperty("assigned_user", Integer.valueOf(user.getID()));
		obj.addProperty("checkout_to_type", "user");
		String postJson = obj.toString();
		
		return postRQ(checkOutURL(asset.getID()), postJson);
	}
	
	public static boolean updateAsset(Asset asset) {
		//updates asset
		JsonObject obj = new JsonObject();
		//if more fields are needed to be updated on Snipe, add here
		obj.addProperty("asset_tag", asset.getTag());
		obj.addProperty("notes", asset.getNotes());
		obj.addProperty("status_id", asset.getStatusLabel().getStatusID());
		obj.addProperty("_snipeit_reserved_4", asset.getCustomFields().isReserved() ? 1 : 0); //converts bool to int
		obj.addProperty("_snipeit_reserved_by_3", asset.getCustomFields().getReservedBy());
		obj.addProperty("_snipeit_date_reserved_2", asset.getCustomFields().getReservedDate());
		String postJson = obj.toString();
		
		//send patch request
		return patchRQ(idURL(asset.getID()), postJson);
	}
	
	public static ArrayList<JsonElement> searchResults(String completeURL){
		//expecting a complete URL complete with search parameters
		ArrayList<JsonElement> results = new ArrayList<JsonElement>();
		//send request
		JsonObject snipeList = getRQ(completeURL).getAsJsonObject();
		JsonArray resultsJArr = snipeList.get("rows").getAsJsonArray();
		//add results to arraylist
		if (resultsJArr.size() > 0) {
			for (int i = 0; i < resultsJArr.size(); i++) {
				results.add(resultsJArr.get(i));
			}
		}
		return results;
	}
	
	public static ArrayList<JsonElement> searchAssets(QueryParams queryParams) {
		//be sure that the QueryParams obj is initialised with the appropriate parameter
		return searchResults(hardwareURL() + queryParams.getParams());
		
	}
	public static ArrayList<JsonElement> searchUsers(QueryParams queryParams) {
		//be sure that the QueryParams obj is initialised with the appropriate parameter
		return searchResults(usersURL() + queryParams.getParams());	
	}
	
	/*************************************************************************************************************/
	//Specialised Snipe functions
	
	public static boolean reserveAsset(String username, String date, Asset asset) throws Exception {
		//reserve particular asset for user at date
		checkDateFormat(date);
		Asset.CustomFields cf = asset.new CustomFields();
		cf.setReserved(true);
		cf.setReservedBy(username);
		cf.setReservedDate(date);
		asset.setCustomFields(cf);
		
		return updateAsset(asset);
	}
	
	public static boolean clearReservation(Asset asset) {
		//clear a reservation for an asset
		Asset.CustomFields cf = asset.new CustomFields();
		cf.setReserved(false);
		cf.setReservedBy("null");
		cf.setReservedDate("null");
		asset.setCustomFields(cf);
		
		return updateAsset(asset);
	}
	
	public static Vector<Asset> checkAvailability(String date, String searchString) throws Exception {
		//Checks laptop availability at a specific date
		//checks to make sure date format is correct
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date queryDate = dateFormat.parse(date);
		
		//get list of collab laptops, then split each row and date
		Vector<Asset> list = new Vector<Asset>(0,1);
		Vector<Asset> laptopArr = scrapeCollabLaptops(searchString);
		
		for (Asset l : laptopArr) {
			Date checkOutDate = l.getCheckOutDate();
			
			if ((checkOutDate == null || queryDate.before(checkOutDate)) && l.isCanBeCheckedOut()) {
				list.add(l);
			}
		}
		
		return list;
	}
	
	public static Vector<Asset> checkReturnables(String username, String searchString) throws ParseException {
		//gets returnable laptops for assigned person
		Vector<Asset> list = new Vector<Asset>(0,1);
		Vector<Asset> laptopArr = scrapeCollabLaptops(searchString);
		for (Asset a : laptopArr) {
			System.out.println(a.getAssignedTo().getUsername());
			//given that the username is the one reserving, and the asset can be checked out
			if (username.equalsIgnoreCase(a.getAssignedTo().getUsername()) && !a.isCanBeCheckedOut()) {
				list.add(a);
			}
		}
		return list;
	}
	
	public static Vector<Asset> scrapeCollabLaptops(String searchString) throws ParseException {
		//Gets a list of all the collab latops
		
		//create query params
		QueryParams qp = new QueryParams(true);
		qp.setSortString("asset_tag");
		qp.setSearchString(searchString);
//		qp.setLimitString("1");
		ArrayList<JsonElement> jsonList = searchAssets(qp); //search assets
		
		Vector<Asset> laptops = new Vector<Asset>(0,1);
		for (JsonElement js : jsonList) {
			int id = 0;
			int tag = 0;
			String username = null;
			Date useDate = null;
			boolean canCheckout = false;
			
			JsonElement user = js.getAsJsonObject().get("assigned_to");
			JsonElement reservedFields = js.getAsJsonObject().get("custom_fields");
			
			Asset laptop = new Asset();
			if (js != null && !js.isJsonNull()) {
				Gson gson = AssetDeserializer.gsonAssetCreator();
				 laptop = gson.fromJson(js, Asset.class);
			}
//			id = js.getAsJsonObject().get("id").getAsInt();
//			tag = Integer.parseInt(js.getAsJsonObject().get("asset_tag").getAsString().replaceAll("\"", ""));
//			
			//Adds username of either who has computer reserved or checked out
			if (user != null && !user.isJsonNull()) { //username of asset checked out
				username = user.getAsJsonObject().get("username").getAsString();
			} else if (!reservedFields.isJsonArray()){ //username of asset reserved
				JsonElement resBy = reservedFields.getAsJsonObject().get("Reserved By");
				if (resBy != null && !resBy.isJsonNull() && resBy.getAsJsonObject().get("value") != null 
						&& !resBy.getAsJsonObject().get("value").isJsonNull()) {
					username = resBy.getAsJsonObject().get("value").getAsString();
				}
			}
			
			//Adds date checked out or reserved
			JsonElement date = js.getAsJsonObject().get("last_checkout");
			//checks if asset is checked out and when
			if (date != null && !date.isJsonNull() && js.getAsJsonObject().get("user_can_checkout").getAsBoolean() == false) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				useDate = dateFormat.parse(date.getAsJsonObject().get("datetime").getAsString().split(" ")[0]);
			//checks date reserved if asset not currently checked out
			} else if (!reservedFields.isJsonArray()){
				JsonElement resDate = reservedFields.getAsJsonObject().get("Date Reserved");
				if (resDate != null && !resDate.isJsonNull() && resDate.getAsJsonObject().get("value") != null 
						&& !resDate.getAsJsonObject().get("value").isJsonNull()) {
					
					String pulledString = resDate.getAsJsonObject().get("value").getAsString();
					if (!pulledString.equalsIgnoreCase("null")) {
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						useDate = dateFormat.parse(resDate.getAsJsonObject().get("value").getAsString());
					}
				}
			}
			
			if (js.getAsJsonObject().get("user_can_checkout").getAsBoolean())
				canCheckout = true;
			//set asset fields
			
			laptop.setCanBeCheckedOut(canCheckout);
			laptop.setCheckOutDate(useDate);
			AssignedTo assign = laptop.new AssignedTo();
			assign.setUsername(username);
			laptop.setAssignedTo(assign);
			laptops.add(laptop);
			
		}
		
		//try write to file
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(searchString + "-Laptops.txt")));
			out.println("ID, AssetTag, Username, Date Reserved/Checked out, Can be Checked out");
			
			for (int i = 0; i < laptops.size(); i++) {
				out.println(laptops.elementAt(i).toString());
			}
			
			out.flush();
			out.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return laptops;
	}
	
	public static void autoCheckOut(String searchString) throws ParseException {
		//automatically checks out reserved assets
		Vector<Asset> assets = scrapeCollabLaptops(searchString);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date today = formatter.parse(formatter.format(new Date()));
		for (Asset a : assets) {
			if (a.isCanBeCheckedOut() && a.getCheckOutDate() != null &&a.getCheckOutDate().equals(today)) {
				User user = getUser(a.getAssignedTo().getUsername());
				checkOut(a, user);
				clearReservation(a);
			} 
		}
	}

}